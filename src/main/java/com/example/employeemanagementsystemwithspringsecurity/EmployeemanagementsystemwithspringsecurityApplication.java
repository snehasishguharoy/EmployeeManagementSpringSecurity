package com.example.employeemanagementsystemwithspringsecurity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmployeemanagementsystemwithspringsecurityApplication {

	public static void main(String[] args) {
		SpringApplication.run(EmployeemanagementsystemwithspringsecurityApplication.class, args);
	}
}
