/**
 * 
 */
package com.example.employeemanagementsystemwithspringsecurity.ctlr;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author sony
 *
 */
@RestController
public class HelloController {
	
	@RequestMapping(value="/hello")
	public String sayHello(){
		return "Hello to the Web";
	}

}
